using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
	[SerializeField] private Transform pos;
	[SerializeField] private float posZ;
	[SerializeField] private float posY;
	
	
	private Vector3 offSet = new Vector3(0, 11, -20);
	
    // Start is called before the first frame update
    void Start()
    {
        transform.position = pos.position + offSet;
    }

    // Update is called once per frame
    void Update()
    {
       transform.position = new Vector3(pos.position.x, posY, posZ) + offSet;
    }
	
	/*public void Follow()
	{
		Vector3 dir = pos.position - transform.position;
		dir = dir.normalized * pos.GetComponent<Player>().speed;
		
		transform.Translate(new Vector3(dir.x, 0, 0));
	}*/
}
