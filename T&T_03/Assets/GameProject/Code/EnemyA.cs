using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EnemyA : MonoBehaviour
{
	[SerializeField] private float _amplitude;
	[SerializeField] private float _frequency;
	[SerializeField] private GameObject effect;
	
	[SerializeField] private GameObject killZone;
	private Vector3 posOffset;
		
	
    // Start is called before the first frame update
    void Start()
    {
        posOffset = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        float sinOffset = _amplitude * Mathf.Sin(Time.time * _frequency);
		
		transform.position = new Vector3(0f + sinOffset, 0.2f, 0f) + posOffset;
    }
	
	void OnTriggerEnter2D(Collider2D collider)
	{
		if (collider.GetComponent<Collider2D>().name == "FireBall(Clone)")
		{
			StartCoroutine(Death());
		}		
	}
	
	IEnumerator Death()
	{
		GetComponent<AudioSource>().enabled = true;
		GetComponent<SpriteRenderer>().enabled = false;
		effect.SetActive(true);
		yield return new WaitForSeconds (0.3f);
		Destroy(gameObject);
	}
}
