using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyB : MonoBehaviour
{
	[SerializeField] private float speed = 2f;
	[SerializeField] private float thrust = 300f;
	[SerializeField] private GameObject effect;
	
	
	
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.left * Time.deltaTime * speed);
    }
	
	void OnCollisionEnter2D(Collision2D collider)
	{
		if (collider.collider.name == "player" || collider.collider.name == "FireBall(Clone)" || collider.collider.name == "StompZone")
		{
			if (collider.collider.name == "player")
			{
				GameObject.Find("player").GetComponent<Rigidbody2D>().AddForce(Vector2.up * thrust);
			}
			
			StartCoroutine(Death());
		}		
	}
	
	IEnumerator Death()
	{
		GetComponent<AudioSource>().enabled = true;
		GetComponent<SpriteRenderer>().enabled = false;
		effect.SetActive(true);
		yield return new WaitForSeconds (0.3f);
		Destroy(gameObject);
	}
	
	
}
