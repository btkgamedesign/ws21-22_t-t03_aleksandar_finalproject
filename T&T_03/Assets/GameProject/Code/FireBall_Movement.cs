using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBall_Movement : MonoBehaviour
{
	[SerializeField] private Transform player;
	private Quaternion playerRotation;
	[SerializeField] private float speed;
	[SerializeField] private bool moveRight;
	[SerializeField] private bool moveLeft;
	
	
	
	
    // Start is called before the first frame update
    void Start()
    {
       if(Input.GetKey("x"))
	   {
		   moveLeft = true;
	   }
	   else if (Input.GetKey("c"))
	   {
		   moveRight = true;
	   }
    }

    // Update is called once per frame
    void Update()
    {
		if (moveRight == true)
		{
			MovingRight();
		}
		else if (moveLeft == true)
		{
			MovingLeft();
		}    
    }
	
	void MovingRight()
	{
		transform.Translate(Vector2.right * speed * Time.deltaTime);
		
		StartCoroutine(Destroy());
	}
	
	void MovingLeft()
	{
		transform.localRotation = Quaternion.Euler(0, 0, 0);
		
		transform.Translate(Vector2.left * speed * Time.deltaTime);
		
		StartCoroutine(Destroy());
	}
	
	IEnumerator Destroy()
	{
		yield return new WaitForSeconds (5f);
		
		Destroy(gameObject);
	}
	
	void OnCollisionEnter2D(Collision2D col)
	{		
		if (col.collider.tag == "Enemy" || col.collider.name == "Walls")
		{
			Destroy(gameObject);
		}			
		
	}
}
