using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class KillPlayer : MonoBehaviour
{
	[SerializeField] private Transform enemy;
	[SerializeField] private GameObject Player;
	[SerializeField] private Player playerScript;
	private bool UpgradeA;
	private bool UpgradeB;
	
	// [SerializeField] private Animator transition;

	private int playerLives;
	private Vector3 spawn;
	[SerializeField] private string sceneName;
	
    // Start is called before the first frame update
    void Start()
    {
        sceneName = SceneManager.GetActiveScene().name;		
		spawn = enemy.transform.position;
		
    }

    // Update is called once per frame
    void Update()
    {
		UpgradeA = playerScript.UpgradeActive_A;
		UpgradeB = playerScript.UpgradeActive_B;
    }
	
	void OnTriggerEnter2D(Collider2D collider)
	{		
		if (collider.GetComponent<Collider2D>().name == "player")
		{
			if (UpgradeA == true || UpgradeB == true)
			{
				GetComponent<AudioSource>().enabled = true;
				
				StartCoroutine(WaitForHit());
				Player.GetComponent<Player>().StopUpgradeImidiately();
				Vector2 impact = collider.GetComponent<Rigidbody2D>().velocity;
				collider.GetComponent<Rigidbody2D>().AddForce((-impact + (Vector2.up * 2)) * 50);
				
			}
			else if (UpgradeA == false && UpgradeB == false)
			{
				Player.GetComponent<Player>().currentLifes--;
				
				if(Player.GetComponent<Player>().currentLifes != 0)
				{				
					SceneManager.LoadScene(sceneName);
					enemy.transform.position = spawn;
				}
				else
				{
					// Time.timeScale = 0;
				
					Player.GetComponent<Player>().currentLifes = playerLives;
					SceneManager.LoadScene("GameOver");			
				}
			}

			
		}
		
		
	}
	// disables the colliders shortly to avoid unwanted collision with player
	IEnumerator WaitForHit()
	{
		enemy.GetComponent<Collider2D>().enabled = false;
		enemy.GetComponent<Rigidbody2D>().gravityScale =0f;		
		GetComponent<Collider2D>().enabled = false;	
		
		yield return new WaitForSeconds (2f);
		
		GetComponent<AudioSource>().enabled = false;
		enemy.GetComponent<Collider2D>().enabled = true;
		enemy.GetComponent<Rigidbody2D>().gravityScale = 1f;		
		GetComponent<Collider2D>().enabled = true;
	}
}
