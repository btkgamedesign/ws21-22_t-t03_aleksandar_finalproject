using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Menu : MonoBehaviour
{	
	void Update()
	{
	
	}

	public void OpenMenu()
	{
		SceneManager.LoadScene("Menu");
	}
	
	public void LoadLevel_1()
	{
		SceneManager.LoadScene("Level1");
	}
	
	public void LoadLevel_2()
	{
		SceneManager.LoadScene("Level2");
	}
	
	public void LoadLevel_3()
	{
		SceneManager.LoadScene("Level2 1");
	}
	
	public void Load_Credits()
	{
		SceneManager.LoadScene("Credits");
	}
	
	public void QuitGame()
	{
		Application.Quit();
	}
	
	public void ResumeGame()
	{
		Time.timeScale = 1f;		
	}
}
