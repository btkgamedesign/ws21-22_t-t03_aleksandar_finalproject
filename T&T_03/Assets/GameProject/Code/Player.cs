using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
	[SerializeField] private float speed = 600f;
	[SerializeField] private float thrust = 400f;
	[SerializeField] private float maxSpeed;
	[SerializeField] private float waitForUpgrade;
	
	[SerializeField] private Rigidbody2D rb2D;
	[SerializeField] private Collider2D cc2D;
	[SerializeField] private Animator animator;
	[SerializeField] private Animator animatorStomp;
	[SerializeField] private GameObject stompZone;
	
	
	[SerializeField] private LayerMask platformLayer;
	
	private int beginningLifes = 2;
	public int currentLifes = 1;
	public bool UpgradeActive_A = false;
	public bool UpgradeActive_B = false;
	[SerializeField] private bool isGrounded = false;
	
	[SerializeField] private Transform fireBall;
	private Vector2 playerPosition;
	
	[SerializeField] private GameObject pauseMenu;
	
	[SerializeField] private GameObject fireBallSound;
	[SerializeField] private GameObject stompSound;

	
	
	
	
    // Start is called before the first frame update
    void Start()
    {
        animator = GameObject.Find("Image").GetComponent<Animator>();
		Time.timeScale = 1;
    }

    // Update is called once per frame
    void Update()
    {
		if (Input.GetKey("left shift"))
		{
			speed = 800;
			maxSpeed = 12;
		}
		else
		{
			speed = 600;
			maxSpeed = 8;
		}
		
		//Controll over movement left/right
        if(Input.GetKey("right"))
		{
			rb2D.AddForce(Vector2.right * Time.deltaTime * speed);			
			transform.localRotation = Quaternion.Euler(0, 0, 0);
		}
				
		if(Input.GetKey("left"))
		{
			rb2D.AddForce(Vector2.left * Time.deltaTime * speed);			
			transform.localRotation = Quaternion.Euler(0, 180, 0);
		}
		
		AnimationTransition();
				
		if (rb2D.velocity.x > maxSpeed)
		{
			rb2D.AddForce(Vector2.left * Time.deltaTime * speed);
		}		
		else if (rb2D.velocity.x < -maxSpeed)
		{
			rb2D.AddForce(Vector2.right * Time.deltaTime * speed);
		}
		
		// check if the player is on the ground
		RaycastHit2D raycastHit = Physics2D.Raycast(cc2D.bounds.center, Vector2.down, cc2D.bounds.extents.y + 0.1f, platformLayer);
		Color rayColor;
		
		if(raycastHit.collider != null)
		{
			isGrounded = true;
		}
		else if (raycastHit.collider == null)
		{
			isGrounded = false;
		}
		
		
		//check if the player can jump
		if (isGrounded == true && Input.GetKeyDown("space"))
		{
			StartCoroutine(Jump());
		}
		
		// checks and activates for shooting upgrade
		if (UpgradeActive_A == true)
		{
			StartCoroutine(UpgradeShoot());
		}
		else if (UpgradeActive_B == true)
		{
			UpgradeStomp();
		}
		
		playerPosition = transform.position;
		
		if (Input.GetKeyDown("escape") && pauseMenu.activeSelf == false)
		{
			Time.timeScale = 0f;
			pauseMenu.SetActive(true);
		}
		else if (Input.GetKeyDown("escape") && pauseMenu.activeSelf == true)
		{
			Time.timeScale = 1f;
			pauseMenu.SetActive(false);
		}
		
		if (transform.position.y < -4.5f)
		{
			Death();
		}

    }
	
	
	
	IEnumerator Jump()
	{	
		GetComponent<AudioSource>().enabled = true;
		rb2D.AddForce(Vector2.up * thrust);
		yield return new WaitForSeconds (0.1f);
		GetComponent<AudioSource>().enabled = false;
	}
	
	// Upgrade A: allows the player to shoot projectiles
	IEnumerator UpgradeShoot()
	{
		transform.localScale = new Vector2(8, 8);		
		StartCoroutine(StopUpgrade());		
		GameObject.Find("Image").GetComponent<SpriteRenderer>().color = Color.red;		
		Vector2 offset = new Vector2(1, 0);
		thrust = 500f;
		
		if (Input.GetKeyDown("c"))
		{
			Instantiate(fireBall, playerPosition + offset, Quaternion.identity);
			fireBallSound.SetActive(true);
			yield return new WaitForSeconds (0.1f);
			fireBallSound.SetActive(false);
		}
		
		if (Input.GetKeyDown("x"))
		{
			Instantiate(fireBall, playerPosition - offset, Quaternion.identity);
			fireBallSound.SetActive(true);
			yield return new WaitForSeconds (0.1f);
			fireBallSound.SetActive(false);
		}
		
	}
	
	void UpgradeStomp()
	{
		transform.localScale = new Vector2(13, 13);
		StartCoroutine(StopUpgrade());
		thrust = 500;
		
		if (isGrounded == false && Input.GetKeyDown("z") || Input.GetKeyDown("y"))
		{
			rb2D.AddForce(Vector2.down * 600);
			StartCoroutine(StompArea());
		}
	}
	
	IEnumerator StompArea()
	{
		yield return new WaitForSeconds (0.3f);
		
		if (isGrounded == true)
		{
			stompSound.SetActive(true);
			animatorStomp.SetBool("Stomp", true);
			yield return new WaitForSeconds (0.2f);
			animatorStomp.SetBool("Stomp", false);
			stompSound.SetActive(false);
		}
		
		stompZone.SetActive(true);		
		yield return new WaitForSeconds (0.3f);
		stompZone.SetActive(false);
		
		
	}
	
	
	// Returns the player to normal state
	IEnumerator StopUpgrade()
	{
		yield return new WaitForSeconds (waitForUpgrade);				
		transform.localScale = new Vector2(6, 6);		
		UpgradeActive_A = false;		
		GameObject.Find("Image").GetComponent<SpriteRenderer>().color = Color.white;
		thrust = 400;
		StopUpgradeImidiately();
	}
	
	// returnes the player to normal after enemy hit
	 public void StopUpgradeImidiately()
	{
		UpgradeActive_A = false;
		UpgradeActive_B = false;
		transform.localScale = new Vector2(6, 6);			
		GameObject.Find("Image").GetComponent<SpriteRenderer>().color = Color.white;
		thrust = 400;
	}
	
	// controlls player animations
	void AnimationTransition()
	{
		if (rb2D.velocity.y < -1.5f)
		{
			animator.SetBool("Falling", true);
		}
		else
		{
			animator.SetBool("Falling", false);
		}
		
		if (rb2D.velocity.y > 1f)
		{
			animator.SetBool("JumpingUp", true);
		}
		else
		{
			animator.SetBool("JumpingUp", false);
		}
		
		if (rb2D.velocity.x != 0f && isGrounded == true)
		{
			animator.SetBool("IsRunning", true);
		}
		else if (isGrounded == false)
		{
			animator.SetBool("IsRunning", false);
		}
		else if (rb2D.velocity.x < 4f || rb2D.velocity.x > -4f)
		{			
			animator.SetBool("IsRunning", false);
		}		
	}
	
	// void PauseGame()
	// {
		// Time.timeScale = 0.5f;
		// pauseMenu.SetActive(true);
	// }
		
	void Death()
	{
		currentLifes--;
		
		if (currentLifes != 0)
		{
			int sceneName = SceneManager.GetActiveScene().buildIndex;
			
		
			SceneManager.LoadScene(sceneName);
		}
		else if (currentLifes == 0)
		{
			SceneManager.LoadScene("GameOver");
		}
	}
	
	
}
