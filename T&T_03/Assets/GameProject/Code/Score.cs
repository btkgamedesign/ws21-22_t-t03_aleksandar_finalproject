using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
	[SerializeField] private GameObject player;
	
	public int CoinCount;
	private string CoinCountText;
	[SerializeField] private Text text;
	
	public int LifesCount;
	private string LifesCountText;
	[SerializeField] private Text textLifes;
	
	[SerializeField] private GameObject soundEffect;
	
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        LifesCount = player.GetComponent<Player>().currentLifes;
		LifesCountText = LifesCount.ToString();
		textLifes.text = LifesCountText;
    }
	
	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.GetComponent<Collider2D>().tag == "Increase")
		{
			soundEffect.SetActive(true);
			CoinCount++;
			CoinCountText = CoinCount.ToString();
			text.text = CoinCountText;
			
			StartCoroutine(StopSound());
			
			Destroy(col.gameObject);
		}
		else if (col.GetComponent<Collider2D>().tag == "Increase2")
		{
			soundEffect.SetActive(true);
			CoinCount += 5;
			CoinCountText = CoinCount.ToString();
			text.text = CoinCountText;
			
			StartCoroutine(StopSound());
			
			Destroy(col.gameObject);
		}		
		else if (col.GetComponent<Collider2D>().tag == "Increase3")
		{
			soundEffect.SetActive(true);
			CoinCount += 10;
			CoinCountText = CoinCount.ToString();
			text.text = CoinCountText;		
			
			StartCoroutine(StopSound());
			
			Destroy(col.gameObject);
		}
	}
	
	IEnumerator StopSound()
	{
		yield return new WaitForSeconds (0.1f);
		soundEffect.SetActive(false);
	}
}
