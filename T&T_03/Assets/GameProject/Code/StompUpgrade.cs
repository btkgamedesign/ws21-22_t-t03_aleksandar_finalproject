using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StompUpgrade : MonoBehaviour
{
	public GameObject _player;
	
	private int PlayerLives;
	
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
	
	void OnTriggerEnter2D(Collider2D col)
	{	
		if (col.GetComponent<Collider2D>().name == "player")
		{			
					
			_player.GetComponent<Player>().StopUpgradeImidiately();			
			_player.GetComponent<Transform>().transform.Translate(new Vector2(0f, 1f));			
			_player.GetComponent<Player>().currentLifes += 1;		
			_player.GetComponent<Player>().UpgradeActive_B = true;					
			StartCoroutine(Sound());	
		}		
	}
	
	IEnumerator Sound()
	{
		GetComponent<AudioSource>().enabled = true;
		yield return new WaitForSeconds (0.2f);
		Destroy(gameObject);
	}
}
