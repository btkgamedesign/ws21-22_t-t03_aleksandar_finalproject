using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PolymorphismLecture
{
	public abstract class Enemy : MonoBehaviour
	{
		[SerializeField] private float _hitpoints;
		[SerializeField] private float _speed;
		
		[SerializeField] private float _maxLeft = -5f;
		[SerializeField] private float _maxRight = 5f;
		
		
		public abstract void Attack();
		
		
		public void Update()
		{
			Move();
		}
		
		public abstract void Move();
		
			//transform.Translate(Vector3.forward * Time.deltaTime * 2);
		
		public abstract void Look();
	}
}