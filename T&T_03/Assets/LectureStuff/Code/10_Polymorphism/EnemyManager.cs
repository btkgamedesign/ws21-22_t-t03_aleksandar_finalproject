using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PolymorphismLecture
{
	public class EnemyManager : MonoBehaviour
	{
		
			
			[SerializeField] private Enemy _regular;
			[SerializeField] private Enemy _ranged;
			[SerializeField] private Enemy _melee;	
			[SerializeField] private Enemy _stationary;	
		
		// Start is called before the first frame update
		void Start()
		{
			_regular.Attack();
			_ranged.Attack();
			_melee.Attack();
			_stationary.Attack();
		}
	}
}