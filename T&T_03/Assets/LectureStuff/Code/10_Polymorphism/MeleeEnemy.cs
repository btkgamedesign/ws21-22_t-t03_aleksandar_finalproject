using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PolymorphismLecture
{
	
	
	public class MeleeEnemy : Enemy
	{
		
		private Transform _playerTransform;
		
		public override void Attack()
		{
			Debug.Log("Swing!");
		}
		
		public override void Look()
		{
			transform.LookAt(_playerTransform);
		}
		
		public override void Move()
		{
			transform.Translate(Vector3.forward * Time.deltaTime);
		}
	}
}