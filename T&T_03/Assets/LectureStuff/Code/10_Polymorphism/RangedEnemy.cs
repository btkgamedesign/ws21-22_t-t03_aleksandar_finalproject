using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PolymorphismLecture
{
	
	
	public class RangedEnemy : Enemy
	{
		
		private Transform _playerTransform;
		
		public override void Attack()
		{
			Debug.Log("Shoot!");
		}
		
		public override void Look()
		{
			transform.LookAt(_playerTransform);
		}
		
		public override void Move()
		{
			transform.Translate(Vector3.forward * Time.deltaTime);
		}
	}
}