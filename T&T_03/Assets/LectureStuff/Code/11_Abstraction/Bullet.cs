using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
	public float speed = 10f;
	
	public int damage = 5;
	public float respawnRate = 1f;

	public GameObject[] bullets;
	
    // Start is called before the first frame update
    void Start()
    {
       bullets = GameObject.FindGameObjectsWithTag("BlackTile");
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.up * Time.deltaTime * speed);
		
		StartCoroutine(TraverDistance());		
    }
	
	IEnumerator TraverDistance()
	{
		yield return new WaitForSeconds (respawnRate);
		
		foreach(GameObject bullet in bullets)
		{
			Destroy(gameObject);
		}
	}
}
