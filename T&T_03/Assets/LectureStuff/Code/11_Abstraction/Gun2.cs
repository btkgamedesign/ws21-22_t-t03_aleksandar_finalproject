using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Gun2 : MonoBehaviour
{
	
    public abstract void Shoot();
	
	public abstract void Reload();
	
	public abstract void Switch();
	
}
