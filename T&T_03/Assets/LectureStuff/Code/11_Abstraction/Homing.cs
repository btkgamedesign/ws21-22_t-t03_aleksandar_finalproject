using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Homing : Bullet
{
	private Transform enemy;
	
	
    // Start is called before the first frame update
    void Start()
    {
        enemy = GameObject.FindWithTag("Enemy").transform;
    }

    // Update is called once per frame
    void Update()
    {
		transform.Translate(Vector3.forward * Time.deltaTime * speed);
		
        transform.LookAt(enemy);
    }
}
