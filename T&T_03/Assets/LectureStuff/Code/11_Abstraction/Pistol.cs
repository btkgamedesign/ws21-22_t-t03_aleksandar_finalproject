using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pistol : Gun2
{
	private Transform projectile;
	
	public Transform bullet;
	public Transform missle;
	public Transform homing;
	
	
	private int magazine = 0;
	public int magazineSize = 13;
	
	public GameObject shotgun;
	private GameObject pistol;
	
    // Start is called before the first frame update
    void Start()
    {
        pistol = GameObject.Find("pistol");
		
		projectile = bullet;
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Mouse0) && magazine < magazineSize)
		{
			Shoot();
			magazine++;
		}
		else if(Input.GetKeyDown("r"))
		{
			Reload();
		}
		else if(Input.GetKeyDown("q"))
		{
			Switch();
		}
		
		if(Input.GetKeyDown("i"))
		{
			projectile = bullet;
		}
		else if(Input.GetKeyDown("o"))
		{
			projectile = missle;
		}
		else if(Input.GetKeyDown("p"))
		{
			projectile = homing;
		}
		
		
    }
	
	public override void Shoot()
	{
		Instantiate(projectile, new Vector3(1f, 1f, -10f), projectile.transform.rotation);
	}
	
	public override void Reload()
	{
		magazine = 0;
	}
	
	public override void Switch()
	{
		shotgun.SetActive(true);
		
		pistol.SetActive(false);
	}
	
	
}
