using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shotgun : Gun2
{
	private Transform projectile;
	
	public Transform bullet;
	public Transform missle;
	public Transform homing;	
	
	private int magazine = 0;
	public int magazineSize = 5;
	
	private int reloadSpeed = 0;
	public float shootSpeed = 0;
	
	private GameObject shotgun;
	public GameObject pistol;
	
    // Start is called before the first frame update
    void Start()
    {
        shotgun = GameObject.Find("shotgun");
		projectile = bullet;
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Mouse0) && magazine < magazineSize && reloadSpeed == 0)
		{
			Shoot();
			magazine++;
		}
		else if(Input.GetKeyDown("r"))
		{
			Reload();
		}
		else if(Input.GetKeyDown("q"))
		{
			Switch();
		}
		
		if(Input.GetKeyDown("i"))
		{
			projectile = bullet;
		}
		else if(Input.GetKeyDown("o"))
		{
			projectile = missle;
		}
		else if(Input.GetKeyDown("p"))
		{
			projectile = homing;
		}
    }
	
	public override void Shoot()
	{
		Instantiate(projectile, new Vector3(1f, 1f, -10f), Quaternion.Euler(new Vector3(Random.Range(85, 95), Random.Range(-5, 5), 0)));
	
		
		reloadSpeed++;
		
		StartCoroutine(Wait());
	}
	
	public override void Reload()
	{
		magazine = 0;
		
		
	}
	
	public override void Switch()
	{		
		pistol.SetActive(true);
		
		shotgun.SetActive(false);
	}
	
	IEnumerator Wait()
	{
		yield return new WaitForSeconds (shootSpeed);
		
		reloadSpeed = 0;
	}
	
	
}
