using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damageable : MonoBehaviour
{
	
	public Transform player;
	public int increaseHP;
	public int increaseMana;
	
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
	
	public void Collect()
	{
		transform.position = new Vector2(Random.Range(-10, 10), Random.Range(-10, 10));
	}
	
	public void UsePotion()
	{
		increaseHP = player.GetComponent<PlayerScript>().MaxHealthPoints + 4;
		increaseMana = player.GetComponent<PlayerScript>().MaxManaPoints + 4;
		Debug.Log("Increase Potential!");
	}
}
