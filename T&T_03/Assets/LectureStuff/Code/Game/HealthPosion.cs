using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPosion : MonoBehaviour
{
	public Transform player;
	public int heal;
	
    // Start is called before the first frame update
    void Start()
    {
		
    }

    // Update is called once per frame
    void Update()
    {
        
    }
	
	public void Collect()
	{
		transform.position = new Vector2(Random.Range(-10, 10), Random.Range(-10, 10));	
	}
	
	public void UsePotion()
	{
		heal = player.GetComponent<PlayerScript>()._currentHP + 2;
		Debug.Log("Heal");
	}
}
