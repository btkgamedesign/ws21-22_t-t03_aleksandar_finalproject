using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManaPotion : MonoBehaviour
{
	public Transform player;
	public int regen;
	
    // Start is called before the first frame update
    void Start()
    {
		
    }

    // Update is called once per frame
    void Update()
    {
        
    }
	
	public void Collect()
	{
		transform.position = new Vector2(Random.Range(-10, 10), Random.Range(-10, 10));
	}
	
	public void UsePotion()
	{
		regen = player.GetComponent<PlayerScript>()._currentMana + 2;
		Debug.Log("Regen");
	}
}
