using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
	//private int _maxHealthPoints = 10;
	public int MaxHealthPoints = 10; 								//=> _maxHealthPoints;
	
	//private int _maxManaPoints = 10;
	public int MaxManaPoints = 10;								//=> _maxManaPoints;
	
	public int _currentHP = 10;
	public int _currentMana = 10;
	
	public Transform manaPotion;
	public Transform HPPotion;
	public Transform DamageablePotion;	
	
	private int manaPotionCount = 0;
	private int HPPotionCount = 0;
	private int DamageablePotionCount = 0;
	
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKey("w"))
		{
			transform.Translate(Vector2.up * Time.deltaTime * 4);
		}
		else if(Input.GetKey("s"))
		{
			transform.Translate(Vector2.down * Time.deltaTime * 4);
		}
		 else if(Input.GetKey("a"))
		{
			transform.Translate(Vector2.left * Time.deltaTime * 4);
		}
		else if(Input.GetKey("d"))
		{
			transform.Translate(Vector2.right * Time.deltaTime * 4);
		}
		
		if(Input.GetKeyDown("1") && HPPotionCount > 0 && _currentHP < MaxHealthPoints)
		{
			HPPotion.GetComponent<HealthPosion>().UsePotion();
			_currentHP = HPPotion.GetComponent<HealthPosion>().heal;
			
			HPPotionCount--;
		}
		else if(Input.GetKeyDown("2") && manaPotionCount > 0 && _currentMana < MaxManaPoints)
		{
			manaPotion.GetComponent<ManaPotion>().UsePotion();
			_currentMana = manaPotion.GetComponent<ManaPotion>().regen;
			
			manaPotionCount--;
		}
		else if(Input.GetKeyDown("3"))
		{
			DamageablePotion.GetComponent<Damageable>().UsePotion();
			MaxHealthPoints = DamageablePotion.GetComponent<Damageable>().increaseHP;
			MaxManaPoints = DamageablePotion.GetComponent<Damageable>().increaseMana;
			
			DamageablePotionCount--;
		}
		
		
    }
	
		void OnCollisionEnter2D(Collision2D collision)
		{
			
			if(collision.gameObject.tag == "HP")
			{
				//_currentHP = HPPotion.GetComponent<HealthPosion>().heal;
				//Debug.Log("Current HP is " + _currentHP);
				
				HPPotion.GetComponent<HealthPosion>().Collect();
				
				HPPotionCount++;
				Debug.Log("Health Postions in posesion: " + HPPotionCount);
			}
			else if(collision.gameObject.tag == "Mana")
			{
				//_currentHP = manaPotion.GetComponent<ManaPotion>().regen;
				//Debug.Log("Current Mana is " + _currentMana);
				
				manaPotion.GetComponent<ManaPotion>().Collect();
				
				manaPotionCount++;
				Debug.Log("Mana Postions in posesion: " + manaPotionCount);
			}
			else if(collision.gameObject.tag == "Increase")
			{
				DamageablePotion.GetComponent<Damageable>().Collect();
				
				DamageablePotionCount++;
				Debug.Log("Increase in HP and Mana with : " + DamageablePotion);
			}
		}
}
