using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrayMultyD : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        SpriteRenderer whiteTile = GameObject.FindWithTag("WhiteTile").GetComponent<SpriteRenderer>();
        SpriteRenderer blackTile = GameObject.FindWithTag("BlackTile").GetComponent<SpriteRenderer>();
		
		SpriteRenderer[,] gameboard = new SpriteRenderer[8, 8];	

		
		for(int y = 0; y < 8; y++)
		{
			for(int x = 0; x < 8; x++)
			{
				Vector3 position = new Vector3(x - 4.5f, y - 4.5f);
				if(y % 2 == 0)
				{
					gameboard[x, y] = Instantiate(x % 2 == 1 ? blackTile : whiteTile, position, Quaternion.identity);
				}
				else
				{
					gameboard[x, y] = Instantiate(x % 2 == 0 ? blackTile : whiteTile, position, Quaternion.identity);
				}
			}
			
			

		}
		
		
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
