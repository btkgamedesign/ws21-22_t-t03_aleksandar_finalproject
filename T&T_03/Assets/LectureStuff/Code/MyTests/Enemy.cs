using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
	[SerializeField] private int hitPoints = 100;
	
	[SerializeField] private int damage = 5;
	
	public float critChance;
	
	[SerializeField] private float _amplitude;
	[SerializeField] private float _frequency;
	
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float sinOffset = _amplitude * Mathf.Sin(Time.time * _frequency);
		
		transform.Translate(new Vector3(sinOffset, 0, 0) * Time.deltaTime);
    }
	
	void OnCollisionEnter(Collision collider)
	{
		Debug.Log("Hit!");
		
		critChance = Random.Range(1, 10);
		
		if (collider.gameObject.tag == "BlackTile")
		{
			InflctDamage();
		}	
	}
	
	void InflctDamage ()
	{
		if (critChance <= 3)
		{
			hitPoints -= damage * 2;
		}
		else
		{
			hitPoints -= damage;
		}
		
		Debug.Log(hitPoints + " remaining!");
	}
}
