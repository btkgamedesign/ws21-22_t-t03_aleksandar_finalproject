using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlowControll : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        int Hit = Random.Range(0, 1000);
		bool Bullseye = Random.Range(0, 1000) == 500;
		bool Jackpot = Random.Range(0, 1000) == 777;
		
		if (Bullseye)
		{
			Debug.Log("Bullseeye");
		}
		
		else if (Jackpot)
		{
			Debug.Log("Jackpot");
		}
		
	 	else if (100 > Hit  &&  950 < Hit)
		{
			Debug.Log("You loose!");
		}

		else if (100 < Hit && 250 > Hit && 800 > Hit && 950 < Hit)
		{
			Debug.Log("You loose!");
		}
		
		else if (101 < Hit && 950 > Hit)
		{
			Debug.Log("You win!");
		}
		
		
		string message = (Random.Range(0, 10) < 7) ? "You Hit" : "You miss";
		Debug.Log(message);
    }

    // Update is called once per frame
    void Update()
    {
		
    }
}
