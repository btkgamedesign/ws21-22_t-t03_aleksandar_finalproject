using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
	public Transform projectile;
	public int bulletNumber = 10;
	int i = 0;
	
    // Start is called before the first frame update
    void Start()
    {
		
    }
	
	

    // Update is called once per frame
    void Update()
    {
		//Destroy(GameObject.FindWithTag("BlackTile"));

		
         if (Input.GetKey(KeyCode.Mouse0) && i < bulletNumber)
		{
			//Debug.Log("Fire!");
			Instantiate(projectile, new Vector3(0.5f, 0.7f, -9f), projectile.transform.rotation);
			//projectile.transform.Translate(Vector3.forward * Time.deltaTime * speed);
			
			i++;
		}
		else
		{
			StartCoroutine(NextBullet ());
		}
    }
	
	
	IEnumerator NextBullet ()
	{
		yield return new WaitForSeconds (0.3f * bulletNumber);
		Destroy(GameObject.FindWithTag("BlackTile"));
		
		i = 0;
		
	}
}
