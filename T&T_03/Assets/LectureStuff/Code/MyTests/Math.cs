using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Math : MonoBehaviour
{
	float x = 3.25f;
	float y = 1.337f;
	
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {	
		
		float time = Time.time;
		float Z = time * x;
		
		Z = Mathf.Sin(Z);
		
		Z *= y;
		
		Z = Mathf.Clamp(Z, -1, 5);
		
		transform.position = new Vector3(0f, Z, 0f);
		
    }
}
